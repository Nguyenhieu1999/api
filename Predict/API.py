from flask import Flask, request, jsonify
from sklearn.externals import joblib
from Predict import Predict
import traceback
import pandas as pd
import numpy as np

# Your API definition
app = Flask(__name__)
MODEL = Predict('MLP_8.joblib')
MODEL.load_model()
MODEL_LABELS = ['1', '0']

@app.route('/predict', methods=['POST'])
def predict():
    if MODEL:
        try:
            json_ = request.json
            print(json_)
            title = request.args.get('title', type=str)
            print(title)
            titlecompare = request.args.get('titlecompare', type=str)
            print(titlecompare)
            features = [title, titlecompare]
            MODEL.load_input_from_json(title, titlecompare)
            label_index = np.array(MODEL.predict())
            percent= np.max(MODEL.predict_proba(), axis = 1)
            final_json = []
            for i in range(label_index.shape[0]):
                final_json.append({'label' : int(label_index[i]), 'percent' : float(percent[i])})
            return jsonify(final_json)
        except:
            return jsonify({'trace': traceback.format_exc()})
    else:
        print ('Train the model first')
        return ('No model here to use')
if __name__ == '__main__':
    try:
        port = int(sys.argv[1])
    except:
        port = 1234
    app.run(port=port, debug=True)   
#http://127.0.0.1:1234/predict?