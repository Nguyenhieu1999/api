from Predict import Predict
import numpy as np
from sklearn.metrics import accuracy_score
import json

model = Predict('MLP_8.joblib')

received_data = str(input('Enter data: ')).split('|')

main_title = received_data[0]

compare_titles = received_data[1]

model.load_model()

model.load_input_from_json(main_title, compare_titles)

model.output_json()
