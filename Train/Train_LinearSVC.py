import sys
import os
import constant

sys.path.append(os.path.abspath(constant.toSubLibpath()))

from Train import TrainBase
from DataPreProcess import train_test_split

from sklearn.metrics import accuracy_score
from sklearn.svm import LinearSVC
from sklearn.multiclass import OneVsRestClassifier

class Training_LinearSVC(TrainBase):
    # OVERRIDE
    def train_to_model(self, ratio=0.8, random_state=0, tol=1e-4,max_iter=100000, penalty='l2', dual=True, loss='hinge', 
        C=1, fit_intercept=False, class_weight='balanced', multi_class='ovr', verbose=0, intercept_scaling=1):
        
        self.ratio = ratio
        
        train_data, test_data, train_labels, test_labels = train_test_split(self.vectors, self.labels, int(self.size_of_data * ratio))
            
        model = OneVsRestClassifier(LinearSVC(random_state=random_state, tol=tol, max_iter=max_iter, 
                            penalty=penalty, dual=dual, loss=loss, 
                            C=C, fit_intercept=fit_intercept, class_weight=class_weight, 
                            multi_class=multi_class, verbose=verbose, intercept_scaling=intercept_scaling), n_jobs=-1).fit(train_data, train_labels)

        self.accuracy = accuracy_score(model.predict(test_data), test_labels)
        self.model = model
