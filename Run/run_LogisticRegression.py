import sys
import os
import constant

sys.path.append(os.path.abspath(constant.toTrainpath()))

from Train_LogisticRegression import Training_LogisticRegression

data_name = str(input("Enter data file name: "))

training = Training_LogisticRegression(data_name)
training.process_data()
training.train_to_model(max_iter=1000)

print(training.print_acc())

save = input('Save model? Y/N ')
if save == 'Y':
    model = input('Enter where to save: ')
    print(training.print_and_save(model))