import sys
import os
import constant

sys.path.append(os.path.abspath(constant.toTrainpath()))

from Train_NaiveBayes import Training_MNB

data_name = str(input("Enter data file name: "))

training = Training_MNB(data_name)
training.process_data()
training.train_to_model(fit_prior=False, alpha=1e-10)

print(training.print_acc())

save = input('Save model? Y/N ')
if save == 'Y':
    model = input('Enter where to save: ')
    print(training.print_and_save(model))